<?php
/**
 * Created by PhpStorm.
 * User: Ezytli
 * Date: 25.09.2017
 * Time: 13:31
 */

namespace exbico\gateway\mfms\containers;

class OutMessageRequest
{
    public $auth;
    public $outMessage = [];

    public function __construct()
    {
        $this->auth = new Auth();
    }

    public function addMessage(OutMessage $outMessage)
    {
        $this->outMessage[] = $outMessage;
    }

    public function cleanMessage()
    {
        $this->outMessage = [];
    }
}
