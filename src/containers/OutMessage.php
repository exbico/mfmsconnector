<?php
/**
 * Created by PhpStorm.
 * User: Ezytli
 * Date: 25.09.2017
 * Time: 13:40
 */

namespace exbico\gateway\mfms\containers;

class OutMessage
{
    public $clientId;
    public $subject;
    public $address;
    public $priority;
    public $startTime;
    public $validityPeriodMinutes;
    public $contentType;
    public $content;
    public $comment;
}
