<?php

namespace exbico\gateway\mfms\adapter;

use exbico\gateway\mfms\containers\OutMessage;
use exbico\gateway\mfms\interfaces\IMessageContainer;

/**
 * Created by PhpStorm.
 * User: Ezytli
 * Date: 28.09.2017
 * Time: 18:03
 */
class OutMessageAdapter
{
    protected $outMessageModel;

    public function __construct()
    {
        $this->outMessageModel = new OutMessage();
    }

    public function adapt(IMessageContainer $container): OutMessage
    {
        $outMessage = clone $this->outMessageModel;
        $outMessage->clientId = $container->getClientId();
        $outMessage->subject = $container->getSubject();
        $outMessage->address = $container->getAddress();
        $outMessage->priority = $container->getPriority();
        $outMessage->startTime = $container->getStartTime();
        $outMessage->validityPeriodMinutes = $container->getValidityPeriodMinutes();
        $outMessage->contentType = $container->getContentType();
        $outMessage->content = $container->getContent();
        $outMessage->comment = $container->getComment();
        return $outMessage;
    }
}
