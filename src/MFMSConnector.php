<?php
/**
 * Created by PhpStorm.
 * User: Ezytli
 * Date: 22.09.2017
 * Time: 18:08
 */

namespace exbico\gateway\mfms;

use exbico\gateway\mfms\adapter\OutMessageAdapter;
use exbico\gateway\mfms\containers\Auth;
use exbico\gateway\mfms\containers\OutMessageRequest;
use exbico\gateway\mfms\helpers\ConsumeOutMessageResponse;
use exbico\gateway\mfms\helpers\MFMSConnectorSoap;
use exbico\gateway\mfms\interfaces\IMessageContainer;

class MFMSConnector
{
    const WSDL = __DIR__ . DIRECTORY_SEPARATOR . 'mfms.wsdl.xml';

    protected $pathToWsdl = self::WSDL;
    protected $arguments;
    protected $message;
    protected $adapter;
    protected $soap;

    public function __construct(Auth $auth)
    {
        $this->arguments = new OutMessageRequest();
        $this->arguments->auth = $auth;
        $this->adapter = new OutMessageAdapter();
    }

    /**
     * @param IMessageContainer $message
     * @return ConsumeOutMessageResponse|\stdClass
     * @throws \SoapFault
     */
    public function sender(IMessageContainer $message)
    {
        $outMessage = $this->adapter->adapt($message);
        $arguments = $this->arguments;
        $arguments->cleanMessage();
        $arguments->addMessage($outMessage);
        return $this->getSoap()->consumeOutMessage($arguments);
    }

    public function setPathToWsdl(string $pathToWsdl)
    {
        $this->pathToWsdl = $pathToWsdl;
    }

    /**
     * @return \SoapClient|MFMSConnectorSoap
     */
    protected function getSoap(): \SoapClient
    {
        if ($this->soap === null) {
            $this->soap = new \SoapClient($this->pathToWsdl, ['trace' => true]);
        }
        return $this->soap;
    }

    /**
     * @return null|string
     */
    public function getRequestHeaders()
    {
        return $this->getSoap()->__getLastRequestHeaders();
    }

    /**
     * @return null|string
     */
    public function getRequestBody()
    {
        return $this->getSoap()->__getLastRequest();
    }

    /**
     * @return null|string
     */
    public function getResponseHeaders()
    {
        return $this->getSoap()->__getLastResponseHeaders();
    }

    /**
     * @return null|string
     */
    public function getResponseBody()
    {
        return $this->getSoap()->__getLastResponse();
    }
}
