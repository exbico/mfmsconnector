<?php
/**
 * Created by PhpStorm.
 * User: Ezytli
 * Date: 22.09.2017
 * Time: 18:53
 */

namespace exbico\gateway\mfms\helpers;

use exbico\gateway\mfms\containers\OutMessageRequest;

abstract class MFMSConnectorSoap extends \SoapClient
{
    /**
     * @param ConsumeOutMessageRequest|\stdClass|OutMessageRequest $params
     * @return mixed
     * @throws \SoapFault
     */
    abstract public function consumeOutMessage($params);

    abstract public function findOutMessage();
}

/**
 * Class ConsumeOutMessageRequest
 * @property ConsumeOutMessageRequest_Auth $auth
 * @property ConsumeOutMessageRequest_OutMessage[] $outMessage
 * @package console\components\gateway\helpers
 */
abstract class ConsumeOutMessageRequest
{
}

/**
 * Class ConsumeOutMessageRequest_Auth
 * @property string $login
 * @property string $password
 * @package console\components\gateway\helpers
 */
abstract class ConsumeOutMessageRequest_Auth
{
}

/**
 * Class ConsumeOutMessageRequest_Auth
 * @property int $clientId;
 * @property string $subject;
 * @property string $address;
 * @property string $priority;
 * @property string $startTime;
 * @property string $validityPeriodMinutes;
 * @property string $contentType;
 * @property string $content;
 * @property string $comment;
 * @package console\components\gateway\helpers
 */
abstract class ConsumeOutMessageRequest_OutMessage
{
}

/**
 * Class ConsumeOutMessageRequest_Auth
 * @property ConsumeOutMessageResponse_Result $outMessageConsumeResult
 * @package console\components\gateway\helpers
 */
abstract class ConsumeOutMessageResponse
{
}

/**
 * Class ConsumeOutMessageRequest_Auth
 * @property string $code
 * @property int $clientId
 * @property int $providerId
 * @package console\components\gateway\helpers
 */
abstract class ConsumeOutMessageResponse_Result
{
    const CODE_OK = 'ok';
}
