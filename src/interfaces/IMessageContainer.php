<?php

namespace exbico\gateway\mfms\interfaces;

/**
 * Created by PhpStorm.
 * User: Ezytli
 * Date: 28.09.2017
 * Time: 18:03
 */

interface IMessageContainer
{

    public function getClientId(): int;

    public function getSubject(): string;

    public function getAddress(): string;

    /**
     * @description [low, normal, high, realtime]
     * @return null|string
     */
    public function getPriority();

    /**
     * @description yyyy-mm-ddThh:mm:ss
     * @return string|null
     */
    public function getStartTime();

    /**
     * @return int|null
     */
    public function getValidityPeriodMinutes();

    /**
     * @description available only text
     * @return string
     */
    public function getContentType(): string;

    public function getContent(): string;

    /**
     * @return null|string
     */
    public function getComment();
}
